﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldCreate : MonoBehaviour
{
    //シールドのプレファブ
    private GameObject shieldPrefab;
    //反射のプレファブ
    private GameObject bouncePrefab;
    //現在選択しているプレファブ
    private GameObject Knight;

    private Vector3 worldCcreenPos;

    // Start is called before the first frame update
    void Start()
    {
        //シールドプレファブの初期化
        shieldPrefab = Resources.Load("Shield") as GameObject;
        if (!shieldPrefab)
        {
            Debug.Log("not shield");
        }

        //反射プレファブの初期化
        bouncePrefab = Resources.Load("Bounse") as GameObject;
        if (!bouncePrefab)
        {
            Debug.Log("not Bounse");
        }

        //Knight = shieldPrefab;
        Knight = shieldPrefab;

        //Debug.Log("width: " + Screen.width + "height: " + Screen.height);
        Vector3 screenPos = new Vector3(Screen.width, Screen.height, 0.0f);
        worldCcreenPos = SetWorledPosition(screenPos);
    }

    // Update is called once per frame
    void Update()
    {
        CreateShield();
    }


    public void CreateShield()
    {
        if (Input.GetMouseButtonDown(0)) //左クリック
        {
            Vector3 clickPosition = SetWorledPosition(Input.mousePosition);
            //画面の範囲内に入ってるかどうか
            if (IsRangeEnter(clickPosition))
            {
                Instantiate(Knight, clickPosition, Quaternion.identity);
            }
        }
    }

    public Vector3 SetWorledPosition(Vector3 position)
    {
        Vector3 worledPos;
        position.z = 10.0f;

        worledPos = Camera.main.ScreenToWorldPoint(position);
        //Debug.Log("clickPos x :" + worledPos.x + " y :" + worledPos.y + " z :" + worledPos.z);
        return worledPos;
    }

    public bool IsRangeEnter(Vector3 clickPos)
    {
        float halfWordScreenPosY = SetWorledPosition(new Vector3(0.0f, Screen.height * 0.5f, 0.0f)).y;
        if (clickPos.x > worldCcreenPos.x || clickPos.x < -worldCcreenPos.x ||
            clickPos.y > halfWordScreenPosY || clickPos.y < -worldCcreenPos.y)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
