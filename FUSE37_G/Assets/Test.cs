﻿using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;


public class Test : MonoBehaviour
{

    [SerializeField] private Slider hpSlider;
    [SerializeField] private Text gameOverText;

    // Use this for initialization
    void Start()
    {

        float maxHp = 100f;
        float nowHp = 100f;


        //スライダーの最大値の設定
        hpSlider.maxValue = maxHp;

        //スライダーの現在値の設定
        hpSlider.value = nowHp;
        gameOverText.enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
        if(hpSlider.value <= 0)
        {
            gameOverText.enabled = true;
        }
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        hpSlider.value -= 20f;
    }


}