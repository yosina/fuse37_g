﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour
{
    [SerializeField] private GameObject shell;
    [SerializeField] ParticleSystem particle;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip audioClip;

    IEnumerator Start()
    {
        while(true)
        {
            while(true)
            {
                Instantiate(shell, transform.position, transform.rotation);
                particle.Play();
                audioSource.PlayOneShot(audioClip);
                yield return new WaitForSeconds(2.0f);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
