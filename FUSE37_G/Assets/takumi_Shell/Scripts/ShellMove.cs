﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//========================================
// 砲弾の動きを制御するスクリプト
//========================================

public class ShellMove : MonoBehaviour
{
    private float shellSpeed;

    // Start is called before the first frame update
    void Start()
    {
        shellSpeed = 10.0f;
        GetComponent<Rigidbody2D>().velocity = transform.up.normalized * shellSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if(!GetComponent<Renderer>().isVisible)
        {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        Destroy(this.gameObject);
    }
}
