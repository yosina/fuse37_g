﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    private float totalTime;
    private int minute;
    private float seconds;
    private float oldSeconds;
    [SerializeField] private Text timerText;
    [SerializeField] private Text winText;

    // Start is called before the first frame update
    void Start()
    {
        minute = 1;
        seconds = 0.0f;
        oldSeconds = 0.0f;
        totalTime = minute + 60 + seconds;
        winText.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(totalTime <= 0f)
        {
            return;
        }

        totalTime = minute * 60 + seconds;
        totalTime -= Time.deltaTime;

        //　再設定
        minute = (int)totalTime / 60;
        seconds = totalTime - minute * 60;

        //　タイマー表示用UIテキストに時間を表示する
        if ((int)seconds != (int)oldSeconds)
        {
            timerText.text = minute.ToString("00") + ":" + ((int)seconds).ToString("00");
        }
        oldSeconds = seconds;
        //　制限時間以下になったらコンソールに『制限時間終了』という文字列を表示する
        if (totalTime <= 0f)
        {
            winText.enabled = true;
            Debug.Log("制限時間終了");
        }
    }
}
