﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reflection : MonoBehaviour
{
    Rigidbody2D rigid;
    float speed = 500;
    // Start is called before the first frame update
    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
        rigid.AddForce(transform.up * speed);
    }

    // Update is called once per frame
    void Update()
    {
    }
}
