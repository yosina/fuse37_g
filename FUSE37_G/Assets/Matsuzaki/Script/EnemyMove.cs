﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    //Camera targetCamera;

    //public Transform targetObj;
    public GameObject obj;
    //Rect rect = new Rect(0,0,10,10);
    int screenwidth = Screen.width;
    int screenheight = Screen.height;
    private Vector3 screen;
    // Start is called before the first frame update
    void Start()
    {
        //targetCamera = GetComponent<Camera>();

        Vector3 screenPos = new Vector3(screenwidth, screenheight, 0);
        screen = SetWorldPosition(screenPos);
    }

    // Update is called once per frame
    void Update()
    {
        EnemyMove1 enemymove = obj.GetComponent<EnemyMove1>();
        if (enemymove.position.x > screen.x +3) {
            enemymove.m_Move = -0.5f;
        }
        else if(enemymove.position.x < -screen.x -3)
        {
            enemymove.m_Move = 0.5f;
        }
    }
    public Vector3 SetWorldPosition(Vector3 position)
    {
        Vector3 worldpos;
        worldpos = Camera.main.ScreenToWorldPoint(position);
        return worldpos;
    }
}
