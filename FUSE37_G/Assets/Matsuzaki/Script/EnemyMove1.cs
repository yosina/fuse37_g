﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove1 : MonoBehaviour
{
    public float m_Move = 0.5f;
    public Vector3 position;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        position = transform.position;
        transform.position = new Vector3(position.x + m_Move * Time.deltaTime, 
                               position.y, 
                               position.z);
    }
}
